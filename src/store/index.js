import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import mutations from './mutations'
import getters from './getters'
import {LocalStorage} from 'quasar'
import daptinClient from '../api'

Vue.use(Vuex);

let appConfig = localStorage.getItem("config");
console.log("Daptin client config endpoint", daptinClient.appConfig.endpoint);
let currentEndpoint = LocalStorage.getItem("DAPTIN_ENDPOINT")

if (appConfig) {
  appConfig = JSON.parse(appConfig)
} else {
  appConfig = "CHANGEME";
}

if (appConfig && appConfig.endpoint && currentEndpoint != appConfig.endpoint) {
  localStorage.setItem("DAPTIN_ENDPOINT", appConfig.endpoint);
  setTimeout(function () {
    window.location = window.location + "";
  }, 200);
}
console.log("App config", appConfig);

var defaultConfig = {
  user: null,
  token: null,
  currentTab: null,
  item: null,
  vars: {},
  pagination: {},
  filters: {},
  path: null,
  type: null,
  loaded: false,
  data: null,
  worldModels: null,
  localData: null,
  currentLayout: null,
  appLayout: appConfig
};

export default function createStore() {
  // daptinClient.worldManager.loadModels();
  console.log("Store created");
  return new Vuex.Store({
    state: defaultConfig,
    actions,
    mutations,
    getters
  })
}
